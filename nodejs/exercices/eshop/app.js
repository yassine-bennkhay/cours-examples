const http = require("http")
const prods = require("./produit")
const url = require("url")
const { join } = require("path")

const fs = require("fs")
let home = fs.readFileSync(join(__dirname, "templates/index.html"), "utf-8")
const style = fs.readFileSync(join(__dirname, "style/style.css"), "utf-8")
const server = http.createServer(function (req, res) {
    const t = req.url.split("/")
    if (req.url == "/") {
        res.writeHead(200)

        let str = ""
        prods.forEach(e => {
            str += `<div class="prduit">
            <img src="../images/${e.image}" alt="">
            <h3>${e.marque}</h3>
            <p>${e.model}</p>
        </div>`
        })
        home = home.replace("{% produits %}", str)
        res.end(home)
    }
    if (req.url == "/style/style.css") {
        res.writeHead(200)
        res.end(style)
    }
    if (t[1] == "images") {
        console.log(t[2])

        res.setHeader("Content-type", "image/jpeg")
        res.writeHead(200)
        fs.readFile(__dirname + "/images/" + t[2], function (er, data) {
            res.write(data)
            res.end()

        })
    }
})

server.listen(3000)