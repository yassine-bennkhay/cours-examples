const express = require('express')

const app = express()
let prods = [{ id: 12, name: "hp" }, { id: 122, name: "dell" }]
app.get('/', (req, res) => {
    res.send("Page Home")
})
app.get('/produits', (req, res) => {
    res.send("Page Produits")
})
app.get('/contact', (req, res) => {
    res.send("page contact")
})
app.get('/api/prods', (req, res) => {

    res.json(prods)
})
app.get('/api/prods/:id', (req, res) => {
    const p = prods.filter(e => e.id == req.params.id);
    if (p.length > 0) res.json(p)
    else res.status(404).send({ success: false, msg: "no Product" })
})
app.use((req, res) => {
    res.status(404).send("page not found")
})
app.listen(4000)


