const express = require("express")

const app = express()
app.use((req, res, next) => {
    console.log(req.hostname)
    //res.send("hi")
    next()
})
app.use(express.static(__dirname + "/public"))


app.use((req, res) => {
    res.status(400).send("NOT FOUND")
})
app.listen(3333)