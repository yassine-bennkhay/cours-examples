const PI = 3.14
function add(a, b) {
    return a + b;
}

function prod(a, b) {
    return a * b;
}

//module.exports = add;  OR module.exports = prod;
module.exports = { add, prod, PI }