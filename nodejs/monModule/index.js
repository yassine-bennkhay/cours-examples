class RandomAge {
    constructor(minAge, maxAge) {
        this.minAge = minAge;
        this.maxAge = maxAge;
    }
    getRandomAge() {
        return Math.floor(Math.random() * (this.maxAge - this.minAge + 1) + this.minAge)
    }
}
const R = 60;
//module.exports = RandomAge
exports.monAge = RandomAge
exports.R = R