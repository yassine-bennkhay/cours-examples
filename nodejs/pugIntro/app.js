const express = require('express')
const pug = require("pug")

const app = express()
app.use(express.static("public"))
app.set("view engine", "pug")
app.set("views", "templates")

app.use((req, res) => {
    res.render("index", { logedin: true, name: "said", age: 22, pl: ["c", "c++", "java"] })
})

app.listen(3333)