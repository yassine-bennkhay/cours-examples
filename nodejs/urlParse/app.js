const http = require('http')
const url = require('url')
const qrs = require('querystring')

const server = http.createServer((req, res) => {
    res.setHeader("Content-type", "application/json")
    res.end(JSON.stringify(qrs.parse(url.parse(req.url).query)))
})

server.listen(3000)