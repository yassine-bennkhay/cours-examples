const http = require("http")
const fs = require("fs")
const url = require("url")
const qs = require("querystring")



const port = 3333
const host = "localhost"
let home = fs.readFileSync(__dirname + "/templates/index.html", "utf-8")
let about = fs.readFileSync(__dirname + "/templates/about.html", "utf-8")
let contact = fs.readFileSync(__dirname + "/templates/contact.html", "utf-8")
const name = process.argv[2]
home = home.replaceAll("{% name %}", name)
about = about.replaceAll("{% name %}", name)
contact = contact.replaceAll("{% name %}", name)


const server = http.createServer(function (req, res) {
    res.setHeader("Content-type", "application/json")
    console.log(req)
    res.end(JSON.stringify(qs.parse(url.parse(req.url).query).id))

    // //let content;
    // switch (req.url) {
    //     case "/":
    //         res.writeHead(200)
    //         res.end(home)
    //         break;
    //     case "/about":
    //         res.writeHead(200)
    //         res.end(about)
    //         break;
    //     case "/contact":
    //         res.writeHead(200)
    //         res.end(contact)
    //         break;
    //     default: res.writeHead(404); res.end("<H1>Page not found</h1>")
    // }
})
server.listen(port, host, () => console.log(port, host))